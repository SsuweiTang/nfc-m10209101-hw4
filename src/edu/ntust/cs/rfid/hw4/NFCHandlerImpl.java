package edu.ntust.cs.rfid.hw4;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.NfcAdapter.CreateNdefMessageCallback;
import android.nfc.NfcAdapter.OnNdefPushCompleteCallback;
import android.nfc.NfcEvent;

/**
 * 
 * @author Ssu-Wei Tang
 *
 */

public class NFCHandlerImpl implements NFCHandler {
	
	private Activity activity;
	private NfcAdapter nfcAdapter;
	private PendingIntent intent;
	private IntentFilter[] filters;
	
	public NFCHandlerImpl(Activity activity)
	{
		this.activity=activity;
		
		//檢查裝置是否有支援NFC
		nfcAdapter=NfcAdapter.getDefaultAdapter(activity.getApplicationContext());
		intent=PendingIntent.getActivity(activity, 0, new Intent(activity,activity.getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
		IntentFilter tagDetected =new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
		filters=new IntentFilter[]{tagDetected};
		
	}

	@Override
	public boolean isSupported() {
		return nfcAdapter!=null;
	}

	@Override
	public boolean isEnable() {
		return nfcAdapter.isEnabled();
	}

	
	@Override
	public void disableForeground() {
		nfcAdapter.disableForegroundDispatch(activity);
	}

	@Override
	public void enableForegrount() {
		nfcAdapter.enableForegroundDispatch(activity, intent, filters, null);
	}
	
	@Override
	public void setNdefPushMessage(NdefMessage message) {
		nfcAdapter.setNdefPushMessage(message, activity);
		
		System.out.println("======setNdefPushMessage======"+message);	
			
	}

	@SuppressLint("NewApi")
	@Override
	public boolean isNDEFPushEnable() {
		// TODO Auto-generated method stub
		return nfcAdapter.isNdefPushEnabled();
	}

}


