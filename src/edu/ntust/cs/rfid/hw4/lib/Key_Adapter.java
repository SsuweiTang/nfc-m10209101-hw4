package edu.ntust.cs.rfid.hw4.lib;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import edu.ntust.cs.rfid.hw4.R;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import edu.ntust.cs.rfid.hw4.lib.KeyContent;;
/**
 *
 * @author Ssu-Wei Tang
 *
 */

public class Key_Adapter extends BaseAdapter {

	private static final int TYPE_BODY = 0;
    private static final int TYPE_HEAD = 1;
    private static final int TYPE_MAX_COUNT = TYPE_HEAD + 1 ;
    private Context mCtx = null;
    private List<KeyContent> mData = new ArrayList<KeyContent>();
    private TreeSet<Integer> mSeparatorsSet = new TreeSet<Integer>();
    public Key_Adapter(Context context, int textViewResourceId){
        this.mCtx = context;  
    }
    
    public void getRecordView(){	
    	notifyDataSetChanged();
    	System.out.println("notifyDataSetChanged");
    }
    

    public void clearlistview(){
    	mData.clear();
    	notifyDataSetChanged();
    }
    
    
    public void addHeadItem(final KeyContent item) {
		mData.add(item);
		mSeparatorsSet.add(mData.size() - 1);
        notifyDataSetChanged();
	}

	public void addBodyItem(final KeyContent item) {
		mData.add(item);
        notifyDataSetChanged();
	}
	
    @Override
    public int getItemViewType(int position) {
        return mSeparatorsSet.contains(position) ? TYPE_HEAD : TYPE_BODY;
    }
    @Override
	public int getCount() {
		return mData.size();
	}
    
    @Override
    public int getViewTypeCount() {
        return TYPE_MAX_COUNT;
    }
    
    @Override
    public boolean isEnabled(int position){
    	if(mSeparatorsSet.contains(position)==true){
    		return false;
    	}else{
    		return true;
    	}
    }

	@Override
	public Object getItem(int position) {
		return mData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	
	@SuppressLint("InflateParams")
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
		KeyHolder holder = null;
    	View v = convertView;
    	int type = getItemViewType(position);
    	
        if (v == null) {
        	holder = new KeyHolder();
        	LayoutInflater vi = (LayoutInflater) mCtx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        	switch (type) {
            case TYPE_BODY:
            	v = vi.inflate(R.layout.listview_key, null);
            	holder.name = (TextView) v.findViewById(R.id.listview_key_tv_name);
            	holder.key = (TextView) v.findViewById(R.id.listview_key_tv_key);
            	
            	break;
            case TYPE_HEAD:
            	v = vi.inflate(R.layout.listview_head_key, null);
            	holder.head = (TextView) v.findViewById(R.id.listview_head_key_tv_keytitle);
            	break;
            }
        	v.setTag(holder);
        }else{
        	holder = (KeyHolder)v.getTag();
        }
        KeyContent o = mData.get(position);
        if (o != null) {
        	if(holder.head != null) {
        		
        		System.out.println("~~~~"+o.getKeytitle());
        		holder.head.setText(o.getKeytitle());        	
        	}
            if (holder.name != null) {
            	holder.name.setText(o.getName());
            	
            }
            if (holder.key != null) {
            	holder.key.setText(o.getKey());
            	
            	
            }
        }
        return v;
    }
	public static class KeyHolder {
		public TextView head;
		public TextView name;
    	public TextView key;
      
    }


}
