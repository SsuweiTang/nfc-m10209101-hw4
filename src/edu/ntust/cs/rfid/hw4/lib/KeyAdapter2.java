package edu.ntust.cs.rfid.hw4.lib;

import java.util.ArrayList;

import edu.ntust.cs.rfid.hw4.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
/**
 * 
 * @author Ssu-Wei Tang
 *
 */
public class KeyAdapter2 extends BaseAdapter {

	private Context ct2;
	private ArrayList<KeyContent> items;

	public KeyAdapter2(Context context, ArrayList<KeyContent> items) {
		this.items = items;
		this.ct2 = context;
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		KeyHolder holder = null;
		View v = convertView;

		if (v == null) {
			holder = new KeyHolder();
			v = LayoutInflater.from(parent.getContext()).inflate(
					R.layout.listview_key, null);
			holder.name = (TextView) v.findViewById(R.id.listview_key_tv_name);
			holder.key = (TextView) v.findViewById(R.id.listview_key_tv_key);
			holder.head = (TextView) v.findViewById(R.id.listview_key_tv_keytitle);

			v.setTag(holder);
		} else {
			holder = (KeyHolder) v.getTag();
		}

		KeyContent o = items.get(position);
		if (o != null) {
			if (holder.head != null) {

				System.out.println("~~~~" + o.getKeytitle());
				holder.head.setText(o.getKeytitle());
			}
			if (holder.name != null) {
				holder.name.setText(o.getName());

			}
			if (holder.key != null) {
				holder.key.setText(o.getKey());

			}
		}
		return v;
	}

	public static class KeyHolder {
		public TextView head;
		public TextView name;
    	public TextView key;

	}

}
