package edu.ntust.cs.rfid.hw4.lib;
/**
 * 
 * @author Ssu-Wei Tang
 *
 */
public class KeyContent {
	
	
	String keytitle="";
	String name="";
	String key="";
	public String getKeytitle() {
		return keytitle;
	}
	public void setKeytitle(String keytitle) {
		this.keytitle = keytitle;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	

}
