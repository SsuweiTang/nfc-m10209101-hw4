package edu.ntust.cs.rfid.hw4;

import java.util.ArrayList;
import java.util.Map;



import edu.ntust.cs.rfid.hw4.lib.AES;
import edu.ntust.cs.rfid.hw4.lib.DES;
import edu.ntust.cs.rfid.hw4.lib.KeyAdapter2;
import edu.ntust.cs.rfid.hw4.lib.KeyContent;
import edu.ntust.cs.rfid.hw4.lib.Key_Adapter;
import edu.ntust.cs.rfid.hw4.lib.RSA;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
/**
 * 
 * @author Ssu-Wei Tang
 *
 */
public class Key extends Activity {

	private DB mDbHelper = new DB(Key.this);
	private ListView listview_key;
	private Button btn_bank;
	private ArrayList<KeyContent> list_key;
	private KeyContent[] keycon;
	private KeyAdapter2 key_adapter2;
	private Key_Adapter key_adapter;
	private byte[] publicKey ;
	private byte[] privateKey;
	private byte[] key2 ;
	
	String strPublicKey="";
	String strPrivateKey="";
	String strSecretKey="";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE); 
		setContentView(R.layout.key);
		mDbHelper.open();
		new pageStart().execute();

	}

	@Override
	public void onDestroy() {
		mDbHelper.close();
		super.onDestroy();
	}

	private class pageStart extends AsyncTask<Void, Integer, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			listview_key = (ListView) findViewById(R.id.listview_key);
			btn_bank = (Button) findViewById(R.id.titlepage_btn_back);
			btn_bank.setOnClickListener(tab_onClickListener);	
		}
		@Override
		protected String doInBackground(Void... arg0) {
			System.out.println("getCheckFromSqlite()=="+getCheckFromSqlite());
			if(getCheckFromSqlite().toString().equals("1"))
			{
				try {
					Cursor cursorKey = mDbHelper.getRSA_Key_data();
					while (cursorKey.moveToNext()) {
						strPublicKey = cursorKey.getString(0);
						strPrivateKey = cursorKey.getString(1);

					}	
					System.out.println("strPublicKey"+strPublicKey);
					System.out.println("strPrivateKey"+strPrivateKey);
					cursorKey.close();
				
				} catch (Exception e) {
					System.out.println("Exception: " + e);

				}
			}
			else
			{
				generateRSA();
			}
			

			list_key = new ArrayList<KeyContent>();
			keycon = new KeyContent[3];
			for (int i = 0; i < keycon.length; i++) {
				keycon[i] = new KeyContent();
			}

			
			strSecretKey=getKeyFromSqlite();
			
			
			keycon[0].setKeytitle("RSA");
			keycon[0].setName("Public Key");
			keycon[0].setKey("Click to show public key");
			keycon[1].setKeytitle("RSA");
			keycon[1].setName("Private Key");
			keycon[1].setKey("Click to show private key");
			keycon[2].setKeytitle("AES");
			keycon[2].setName("Secret Key");
			if (strSecretKey.toString().equals(""))
				keycon[2].setKey("Click to generate secret key");
			else
				keycon[2].setKey(strSecretKey);

			for (int i = 0; i < keycon.length; i++) {

				list_key.add(keycon[i]);
			}

			return null;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			key_adapter = new Key_Adapter(Key.this, R.layout.key);
			for (int i = 0; i < list_key.size(); i++) {

				System.out.println("-------------" + list_key.size()
						+ list_key.get(i).getName());
				if (i % 2 == 0) {
					key_adapter.addHeadItem(list_key.get(i));
				}
				key_adapter.addBodyItem(list_key.get(i));
			}

			// key_adapter2=new KeyAdapter2(Key.this,list_key);
			System.out.println(list_key.size() + "4546" + key_adapter);

			if (key_adapter != null)
				listview_key.setAdapter(key_adapter);
			// else
			// System.out.println("key_adapter==null");
			listview_key.setOnItemClickListener(keyItemClick);

		}
		@Override
		protected void onCancelled() {
			super.onCancelled();
		}
	}
	
	private void generateRSA() {
		try {
//			Map<String, Object> keyMap = RSA.generateKeyPair();
//			publicKey = RSA.getPublicKey(keyMap);
//			privateKey = RSA.getPrivateKey(keyMap);
//			insertRSAKeyToSqlite(Base64.encodeToString(privateKey,
//					Base64.DEFAULT),Base64.encodeToString(publicKey,
//							Base64.DEFAULT));
			
			String publickey="",privatekey="";
			privatekey="MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAI2CBh0Qe/fUpkmjvwupQawZrR2ZUlY74Eftl5gsG0pI1Gs55x9WzBsnThQ+gbAxcGB4Mi2ienWJ9yhh2iee83RP8QcVb7e/pt+mNoSkjhESYzYQx16zE5D4GKLdncjbxMtAs9djiy4XXJihebOSvkjBFBpX6VO8nCe8hSZCWfB1AgMBAAECgYA7kgIK2DanxPBfzbwOJ6mKzqBO9o1v2fL3tLB9kgvHANPTc+O1ELAJukknQo0L5DQfmFnfVifhNTxD1rWkukhYDPB48IWzNn5+6kHnBxW/KBNMBzK123W3jplmVmMrCExSN5bfYfiD35dUU9bUhcCOB821D9IeCQ+UbWCnFkcxgQJBAMwDlGKi+9jJZJKwKxeY/QMBk7AcgvROwzwCYNdS4+B8kQygS35J533p6aTXiehIKNHXusxqvfi4cJOKADIRjZUCQQCxkP4bakt+VHi9gBVzowo51pFrkD7abwABd4WIHtfF7JxD7vWaojvv0MrjLfUtl6bp5x5TpsPChRCEkIvpRV9hAkBq7IVcnOc523NmngY3u+r/JFhvOMI5oP4Z+XUf5UWhZifvYiOu8LTu/SWDS6PvpcIfGN3CPT+Ll78I1I79rs1xAkAEDlTPmYD59MIBSiCzUIEWSyFtPJFC+hAc+HbzYgj5u2riXl3710LZOoK357/W98TXSPE3ZfLIybNT491eSbhhAkEAlNhZi7X8HsMZkM9eobZM7A2AFYmVbvO8OD5A4zGCBHt8NWtoTTG1tC5dsDvi2soPHpYCaLNeET0N477onSdVtA==";
			publickey="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCNggYdEHv31KZJo78LqUGsGa0dmVJWO+BH7ZeYLBtKSNRrOecfVswbJ04UPoGwMXBgeDItonp1ifcoYdonnvN0T/EHFW+3v6bfpjaEpI4REmM2EMdesxOQ+Bii3Z3I28TLQLPXY4suF1yYoXmzkr5IwRQaV+lTvJwnvIUmQlnwdQIDAQAB";
			insertRSAKeyToSqlite(privatekey,publickey);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private OnItemClickListener keyItemClick = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> a, View v, int position, long id) {
			
			final TextView name=(TextView)v.findViewById(R.id.listview_key_tv_name);
			final TextView key=(TextView)v.findViewById(R.id.listview_key_tv_key);
			if(name.getText().toString().trim().equals("Public Key"))
			{
				AlertDialog.Builder dialog = new AlertDialog.Builder(Key.this);
				dialog.setTitle(name.getText().toString());
				dialog.setMessage(strPublicKey);
				dialog.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialoginterface, int i) {
								
							}
						});
				dialog.show();	
			}
			if(name.getText().toString().trim().equals("Private Key"))
			{
				AlertDialog.Builder dialog = new AlertDialog.Builder(Key.this);
				dialog.setTitle(name.getText().toString());
				dialog.setMessage(strPrivateKey);
				dialog.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialoginterface, int i) {
								
							}
						});
				dialog.show();
			}
			if(name.getText().toString().trim().equals("Secret Key"))
			{
				AlertDialog.Builder dialog = new AlertDialog.Builder(Key.this);
				dialog.setTitle(name.getText().toString());
				dialog.setMessage("Generate a new key");
				dialog.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialoginterface, int i) {
								try {
									key2 = AES.generateKey();
								} catch (Exception e) {
									e.printStackTrace();
								}
								System.out.println("KEY=" + key2);
								System.out.println("KEY=" + Base64.encodeToString(key2, Base64.DEFAULT));
								insertKeyToSqlite(Base64.encodeToString(key2,Base64.DEFAULT));
								key.setText(Base64.encodeToString(key2,	Base64.DEFAULT));		
							}
						});
				dialog.setNegativeButton("Cancel", null);
				dialog.show();	
			}

		}
	};

	private Button.OnClickListener tab_onClickListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent();
			switch (v.getId()) {
			case R.id.titlepage_btn_back:
				finish();
				break;

			}

		}
	};
	
	public String getCheckFromSqlite() {
		String mycheck = "";
		try {
			Cursor cursorKey = mDbHelper.getCheck_data();
			while (cursorKey.moveToNext()) {
				mycheck = cursorKey.getString(0);
			}
			cursorKey.close();
			System.out.println("sqlite Check:" + mycheck);
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		}
		return mycheck;
	}

	public String getKeyFromSqlite() {
		String mykey = "";
		try {
			Cursor cursorKey = mDbHelper.getKey_data();
			while (cursorKey.moveToNext()) {
				mykey = cursorKey.getString(0);
			}
			cursorKey.close();
			System.out.println("sqlite Key:" + mykey);
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		}
		return mykey;
	}

	private void insertKeyToSqlite(String key) {
		mDbHelper.beginTransaction();
		try {
			System.out.println("=====================key=========" + key);
			mDbHelper.deleteKey_data();
			mDbHelper.insertKey_data(key);
			Toast.makeText(Key.this, "key have been saved", Toast.LENGTH_LONG).show();
			mDbHelper.transactionSuccessful();
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		} finally {
			mDbHelper.endTransaction();
		}

	}

	private void insertRSAKeyToSqlite(String privateKey, String publicKey) {
		mDbHelper.beginTransaction();
		try{
					
			strPrivateKey = privateKey;
			strPublicKey = publicKey;	
			System.out.println("=======privateKey=========" + privateKey);
			System.out.println("=======publicKey=========" + publicKey);
			mDbHelper.deleteKey_data();
			mDbHelper.insert_RSA_Key_data(publicKey, privateKey);
			mDbHelper.deleteCheck_data();
			mDbHelper.insertCheck_data("1");

			mDbHelper.transactionSuccessful();
		} catch (Exception e) {

			System.out.println("Exception: " + e);
		} finally {
			mDbHelper.endTransaction();
		}

	}

}
