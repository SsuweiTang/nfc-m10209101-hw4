### NFC P2P 模式結合對稱式與非對稱式加密 App (Android) ###

* Beaming Message
* 將文字訊息經過對稱式金鑰加密後建立TNF_WELL_KNOWN with RTD_TEXT 型態的Record。
* 將使用者的對稱式金鑰使用RSA公鑰加密後建立TNF_WELL_KNOWN with RTD_TEXT 型態的Record。
* 將加密訊息與加密金鑰透過Android Beam 功能發送NDEFMessage。

* Receiving Message 
* 透過Android Beam 功能接收NDEFMessage。
* 加密的金鑰使用RSA私鑰解出發送端的對稱式金鑰。
* 使用解密後的對稱式金鑰解開密文(ciphertext) 後產生明文(plaintext)。